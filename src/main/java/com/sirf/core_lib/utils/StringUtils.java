package com.sirf.core_lib.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

@Slf4j
public class StringUtils {

    public static String generateMessage(String entity, Map<String, String> searchParams) {
        return String.format("%s was not found for parameters  %s", org.apache.commons.lang3.StringUtils.capitalize(entity), searchParams);
    }

    public static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
        if (entries.length % 2 == 1){
            throw new IllegalArgumentException("Invalid entries");
        }

        return IntStream.range(0, entries.length / 2)
                .map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
                        Map::putAll);
    }

    public static <T> T requireValue(T obj, String message) {

        if (obj == null){
            log.error(message);
            throw new NullPointerException(message);
        }

        if(obj instanceof String && !hasValue((String) obj)) {
            log.error(message);
            throw new NullPointerException(message);
        }
        return obj;
    }

    public static final boolean hasValue(String obj){
        return obj!=null && obj.trim() != "" && obj.trim().length() > 0;
    }
}
