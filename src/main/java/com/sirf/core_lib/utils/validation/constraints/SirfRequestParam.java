package com.sirf.core_lib.utils.validation.constraints;


import com.sirf.core_lib.utils.validation.validators.SirfRequestParamValidator;
import com.sirf.core_lib.utils.validation.constraints.SirfRequestParam.List;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {SirfRequestParamValidator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface SirfRequestParam {

    String message() default "{sirf.constraint.blank }";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean required() default true;
    String name() default "";

    @OverridesAttribute(constraint = NotBlank.class, name = "message") String blankParamMesage() default "{sirf.constraint.blank}";

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    public @interface List {
        SirfRequestParam[] value();
    }
}