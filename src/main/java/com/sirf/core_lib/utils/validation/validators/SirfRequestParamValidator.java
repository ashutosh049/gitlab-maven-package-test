package com.sirf.core_lib.utils.validation.validators;


import com.sirf.core_lib.utils.validation.constraints.SirfRequestParam;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SirfRequestParamValidator implements ConstraintValidator<SirfRequestParam, Object> {

    private boolean required;

    @Override
    public void initialize(SirfRequestParam sirfRequestParam) {
        this.required = sirfRequestParam.required();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        if(required){
            CharSequence charSequence = String.valueOf(value);
            if ( charSequence == null ) {
                return false;
            }
            return charSequence.toString().trim().length() > 0;
        }
        return true;
    }
}