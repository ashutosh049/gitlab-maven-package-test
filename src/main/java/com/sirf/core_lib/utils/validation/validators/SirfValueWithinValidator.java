package com.sirf.core_lib.utils.validation.validators;

import com.sirf.core_lib.utils.validation.constraints.SirfValueWithin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * A generic validator for range of values. The annotated {@link SirfValueWithin#argument()} must be among the {@Link
 * SirfValueWithin#accepts()}
 *
 * @see SirfValueWithin
 */
public class SirfValueWithinValidator implements ConstraintValidator<SirfValueWithin, CharSequence> {

    private static final Logger logger = LoggerFactory.getLogger(SirfValueWithinValidator.class);

    private String[] accepts;

    @Override
    public void initialize(SirfValueWithin parameters) {
        accepts = parameters.accepts();
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext constraintValidatorContext) {
        if(value !=null && value.toString().trim().length() > 0)
            return Arrays.asList(accepts).contains(value);
        return false;
    }


}
