package com.sirf.core_lib.utils.validation.constraints;


import com.sirf.core_lib.utils.validation.constraints.SirfValueWithin.List;
import com.sirf.core_lib.utils.validation.validators.SirfValueWithinValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * A generic validator for range of values. The annotated {@link #name()} must be among the {@Link #accepts()}
 *
 */
@Documented
@Constraint(validatedBy = {SirfValueWithinValidator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface SirfValueWithin {

    String message() default "{sirf.value.within.invalid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String name() default "";

    /**
     * @return Applicable values to be validated against
     */
    String[] accepts() default {};

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    public @interface List {
        SirfValueWithin[] value();
    }

}