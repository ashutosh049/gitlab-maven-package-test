package com.sirf.core_lib.exception;

public class SirfUserApiException extends RuntimeException {

    public SirfUserApiException(String message) {
        super(message);
    }
}