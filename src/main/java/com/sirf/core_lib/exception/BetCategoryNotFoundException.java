package com.sirf.core_lib.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BetCategoryNotFoundException extends NotFoundException {

    public BetCategoryNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public BetCategoryNotFoundException(String msg) {
        super(msg);
    }

    public BetCategoryNotFoundException(String key, String value) {
        super(key, value);
    }

    public BetCategoryNotFoundException(String key, String value, List<Error> errors) {
        super(key, value, errors);
    }
}