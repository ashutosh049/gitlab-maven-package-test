package com.sirf.core_lib.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityExistsException;
import java.util.List;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SirfEntityConflictException extends EntityExistsException {

    private List<Error> errors;

    public SirfEntityConflictException(String message) {
        super(message);
    }

    public SirfEntityConflictException(String message, List<Error> errors) {
        super(message);
        this.errors = errors;
    }

    public SirfEntityConflictException(List<Error> errors) {
        this.errors = errors;
    }

    public SirfEntityConflictException(Class<?> clazz, Object field, Object value) {
        super(String.format("%s already exists with %s=%s", clazz.getSimpleName(), field, value));
    }

}