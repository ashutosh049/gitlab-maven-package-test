package com.sirf.core_lib.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SirfFileNotFoundException extends RuntimeException {
    public SirfFileNotFoundException(String message) {
        super(message);
    }

    public SirfFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}