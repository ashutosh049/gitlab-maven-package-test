package com.sirf.core_lib.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BuyInNotFoundException extends NotFoundException {

    public BuyInNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public BuyInNotFoundException(String msg) {
        super(msg);
    }

    public BuyInNotFoundException(String key, String value) {
        super(key, value);
    }

    public BuyInNotFoundException(String key, String value, List<Error> errors) {
        super(key, value, errors);
    }
}