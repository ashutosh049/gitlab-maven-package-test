package com.sirf.core_lib.exception;

public class EmailConflictException extends SirfEntityConflictException {

    public EmailConflictException(String message) {
        super(message);
    }

    public EmailConflictException(Class<?> clazz, Object field, Object value) {
        super(clazz, field, value);
    }
}
