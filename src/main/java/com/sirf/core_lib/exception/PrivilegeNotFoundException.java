package com.sirf.core_lib.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PrivilegeNotFoundException extends NotFoundException {

    public PrivilegeNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public PrivilegeNotFoundException(String msg) {
        super(msg);
    }

    public PrivilegeNotFoundException(String key, String value) {
        super(key, value);
    }

    public PrivilegeNotFoundException(String key, String value, List<Error> errors) {
        super(key, value, errors);
    }
}