package com.sirf.core_lib.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

import static com.sirf.core_lib.utils.StringUtils.generateMessage;
import static com.sirf.core_lib.utils.StringUtils.toMap;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SirfEntityNotFoundException extends EntityNotFoundException {

    public SirfEntityNotFoundException(String id) {
        super(String.format("No entity was found against parameter %s", id));
    }

    public SirfEntityNotFoundException(Class<?> clazz, Object id) {
        super(String.format("%s was not found for parameter %s", clazz.getSimpleName(), id));
    }

    public SirfEntityNotFoundException(Class<?> clazz, String... searchParamsMap) {
        super(generateMessage(clazz.getSimpleName(), toMap(String.class, String.class, searchParamsMap)));
    }


}