package com.sirf.core_lib.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Builder
public class ApiValidationError extends Error {
    @JsonIgnore
    private String objectName;
    private String field;
    private Object rejectedValue;
    private String message;

    public ApiValidationError(String objectName) {
        this.objectName = objectName;
    }

    public ApiValidationError(String objectName, String message) {
        this.objectName = objectName;
        this.message = message;
    }

    public ApiValidationError(String objectName, String message, String field, Object rejectedValue) {
        this.objectName = objectName;
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
    }

    public static ApiValidationError buildApiValidationError(final String message_, final String field_, final String rejectedValue_){
        return ApiValidationError.builder()
                .message(message_)
                .field(field_)
                .rejectedValue(rejectedValue_)
                .build();
    }
}