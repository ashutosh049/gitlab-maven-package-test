package com.sirf.core_lib.exception;

public class UsernameConflictException extends SirfEntityConflictException {

    public UsernameConflictException(String message) {
        super(message);
    }

    public UsernameConflictException(Class<?> clazz, Object field, Object value) {
        super(clazz, field, value);
    }
}
