/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.BuyIn;
import com.sirf.core_lib.persistence.model.Game;
import com.sirf.core_lib.persistence.model.SirfUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BuyInRepository extends JpaRepository<BuyIn, String> {
    Optional<List<BuyIn>> findAllByGameEquals(Game game);
    Optional<List<BuyIn>> findAllByGameEqualsAndBuyInSirfUserEquals(Game game, SirfUser sirfUser);
}
