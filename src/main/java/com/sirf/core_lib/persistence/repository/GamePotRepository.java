package com.sirf.core_lib.persistence.repository;


import com.sirf.core_lib.persistence.model.BetCategory;
import com.sirf.core_lib.persistence.model.Game;
import com.sirf.core_lib.persistence.model.GamePot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GamePotRepository extends JpaRepository<GamePot, Long> {

    Optional<GamePot> findGamePotByGamePotGameEqualsAndGamePotBetCategoryEquals(Game game, BetCategory betCategory);
    Optional<List<GamePot>> findAllByGamePotGameEquals(Game game);

}
