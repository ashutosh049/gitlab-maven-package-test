package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.SirfUserAffiliationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SirfUserAffiliationCodeRepository extends JpaRepository<SirfUserAffiliationCode, String> {

    Optional<SirfUserAffiliationCode> findByAffiliationCodeEquals(final String affltnCode);

}