package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.Role;
import com.sirf.core_lib.persistence.model.SirfFABGroup;
import com.sirf.core_lib.persistence.model.SirfUser;
import com.sirf.core_lib.persistence.model.SirfUserAffiliationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<SirfUser, String> {
    Optional<SirfUser> findUserByUserIdEquals(String userId);
    Optional<SirfUser> findUserByEmailEquals(String email);
    Optional<SirfUser> findUserByUserNameEquals(String userName);
    Optional<List<SirfUser>> findAllBySirfFABGroupEquals(SirfFABGroup sirfFABGroup);
    Optional<List<SirfUser>> findAllByRolesIn(List<Role> roles);
    Optional<SirfUser> findUserBySirfUserAffiliationCodeEquals(SirfUserAffiliationCode sirfUserAffiliationCode);
}
