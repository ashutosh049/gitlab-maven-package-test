/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.Bet;
import com.sirf.core_lib.persistence.model.BetCategory;
import com.sirf.core_lib.persistence.model.Game;
import com.sirf.core_lib.persistence.model.SirfUser;
import com.sirf.core_lib.persistence.model.types.GameSuperType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BetRepository extends JpaRepository<Bet, Long> {

    Optional<List<Bet>> findAllByBetGameEquals(Game game);

    Optional<List<Bet>> findAllByBetGameEqualsAndBetSirfUserEqualsAndGameSuperTypeEquals(Game game, SirfUser sirfUser, GameSuperType gameSuperType);

    Optional<List<Bet>> findAllByBetGameEqualsAndBetBetCategoryEquals(Game game, BetCategory betCategory);

    Optional<List<Bet>> findAllByBetGameEqualsAndBetBetCategoryEqualsAndStmtSelectionEquals(Game game, BetCategory betCategory, boolean selection);

    Optional<List<Bet>> findAllByBetSirfUserEquals(SirfUser sirfUser);

    Optional<List<Bet>> findBetsByBetSirfUserEqualsAndGameSuperTypeInOrderByGameSuperTypeAsc(SirfUser sirfUser, List<GameSuperType> gameSuperTypes);

    Optional<List<Bet>> findBetsByBetSirfUserEqualsAndGameSuperTypeIn(SirfUser sirfUser, List<GameSuperType> gameSuperTypes, Sort sort);

    @Query("SELECT DISTINCT b.gameSuperType FROM Bet b WHERE b.betSirfUser=:betSirfUser")
    Optional<List<GameSuperType>> getDistinctGameSuperTypesByUser(@Param("betSirfUser") SirfUser betSirfUser);

}
