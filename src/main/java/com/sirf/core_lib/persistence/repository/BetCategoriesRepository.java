package com.sirf.core_lib.persistence.repository;

import java.util.Optional;

import com.sirf.core_lib.persistence.model.BetCategory;
import com.sirf.core_lib.persistence.model.Game;
import com.sirf.core_lib.persistence.model.types.BetSlab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BetCategoriesRepository extends JpaRepository<BetCategory, Long> {

    @Query("SELECT DISTINCT bc1 " +
            "FROM BetCategory bc1 " +
            "JOIN Bet b1 " +
            "ON bc1.betCategoryId = b1.betBetCategory.betCategoryId " +
            "WHERE b1.betGame = :game")
    List<BetCategory> findBetCatByBetGame(@Param("game") Game game);

    //Optional<List<BetCategory>> findAllByGameIdEqualsAndBetSlabEqualsAndTeamIdEquals(String gameId, List<String> betStatementRefernces);
    Optional<List<BetCategory>> findAllByGameIdEqualsAndBetSlabIn(String gameId, List<BetSlab> betSlabs);

}
