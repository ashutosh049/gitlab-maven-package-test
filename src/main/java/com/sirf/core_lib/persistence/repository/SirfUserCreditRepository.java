package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.SirfUserCredit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SirfUserCreditRepository extends JpaRepository<SirfUserCredit, Long> {
}
