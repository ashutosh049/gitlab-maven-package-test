package com.sirf.core_lib.persistence.repository;


import com.sirf.core_lib.persistence.model.Game;
import com.sirf.core_lib.persistence.model.types.GameBetStatus;
import com.sirf.core_lib.persistence.model.types.GameStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameRepository extends JpaRepository<Game, String> {

    List<Game> findGamesByGameStatusEquals(GameStatus gameStatus);
    Optional<List<Game>> findGamesByGameStatusEqualsAndGameBetStatusEquals(GameStatus gameStatus, GameBetStatus gameBetStatus);
    Optional<List<Game>> findTop5ByGameStatusEqualsAndGameBetStatusEqualsAndPlatformIdEquals(GameStatus gameStatus, GameBetStatus gameBetStatus, String platformId);
    Optional<List<Game>> findTop5ByGameStatusEqualsAndPlatformIdEqualsAndGameBetStatusInOrderByGameStartTimeDesc(GameStatus gameStatus, String platformId, List<GameBetStatus> gameBetStatusList);
    Optional<List<Game>> findTop5ByGameStatusEqualsAndGameBetStatusInOrderByPlatformIdAscGameStartTimeDesc(GameStatus gameStatus, List<GameBetStatus> gameBetStatusList);
    Optional<List<Game>> findTop20ByGameStatusEqualsAndGameBetStatusEqualsAndPlatformIdEqualsOrderByGameStartTime(GameStatus gameStatus, GameBetStatus gameBetStatus, String platformId);
    Optional<List<Game>> findTop20ByGameStatusEqualsAndGameBetStatusEqualsAndPlatformIdEqualsAndGameStartTimeAfterAndGameStartTimeBefore(GameStatus gameStatus, GameBetStatus gameBetStatus, String platformId, long startTime, long endTime);
    List<Game> findGamesByGameStatusEqualsAndGameStartTimeBetween(GameStatus gameStatus, long fromEpoch , long toEpoch);
    Optional<Game> findGamesByGameIdEquals(long gameid);
    Optional<List<Game>> findGamesByGameIdIn(List<String> gameIds);

}
