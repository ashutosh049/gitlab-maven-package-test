package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByRoleNameEquals(String roleName);
    Optional<List<Role>> findAllByRoleNameIn(List<String> roleNames);
}
