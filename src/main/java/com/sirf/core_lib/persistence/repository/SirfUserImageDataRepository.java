package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.SirfUser;
import com.sirf.core_lib.persistence.model.SirfUserImageData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SirfUserImageDataRepository extends JpaRepository<SirfUserImageData, String> {

    Optional<SirfUserImageData> findBySirfUserImageDataSirfUserEqualsAndImageTypeEquals(SirfUser sirfUser, String profileType);
    Optional<SirfUserImageData> findBySirfUserImageDataSirfUserEqualsAndUploadIdEquals(SirfUser sirfUser, String uploadId);
}
