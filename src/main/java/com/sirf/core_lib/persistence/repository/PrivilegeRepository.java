package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.Privilege;
import com.sirf.core_lib.persistence.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, Integer> {

    Optional<Privilege> findByPrivilegeNameEquals(String privilegeName);
    Optional<List<Privilege>> findAllByRolesIn(List<Role> roles);
}
