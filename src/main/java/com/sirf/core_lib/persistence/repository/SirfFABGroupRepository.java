package com.sirf.core_lib.persistence.repository;

import com.sirf.core_lib.persistence.model.SirfFABGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SirfFABGroupRepository extends JpaRepository<SirfFABGroup, Long> {

    Optional<SirfFABGroup> findByGroupNameEquals(String groupName);
}
