/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model.types;

import java.math.BigDecimal;

public enum Standings {
    FIRST(new BigDecimal("50"), "1ST PLACE"),
    SECOND(new BigDecimal("30"),"2ND PLACE"),
    THIRD(new BigDecimal("20"),"3RD PLACE"),
    UNRANKED(new BigDecimal("0"),"NOT PLACED");

    private BigDecimal sharePercentage;
    private String description;

    Standings(BigDecimal bigDecimal) {
    }

    public BigDecimal getSharePercentage() {
        return sharePercentage;
    }

    public String getDescription() {
        return description;
    }

    Standings(BigDecimal sharePercentage, String description) {
        this.sharePercentage = sharePercentage;
        this.description = description;
    }
}
