package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.BetStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "bet_trn_publisher")
public class BetTransactionPublisher extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bet_pub_id", updatable = false, nullable = false)
    private Long betPublisherId;

    @ManyToOne
    @JoinColumn(name = "sirf_bet_id")
    @JsonIgnore
    private Bet betBetTransactionPublisher;

    @ManyToOne
    @JoinColumn(name = "sirf_user_id")
    @JsonIgnore
    private SirfUser sirfUserBetTransactionPublisher;

    @Enumerated(EnumType.STRING)
    private BetStatus betStatus;

    @ManyToOne
    @JoinColumn(name = "sirf_user_credit_id")
    @JsonIgnore
    private SirfUserCredit sirfUserCreditBetTransactionPublisher;

    @Column(nullable = false)
    private Boolean betWon;

    @Column(nullable = false)
    private BigDecimal placedAmount;

}
