package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "bet_game_stat")
public class BetGameStat extends AbstractTimestampEntity {

    @Id
    @Column(name = "game_id")
    private String gameId;

    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "game_id")
    private Game game;

    private BigDecimal totalBetAmount;

}
