package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "sirf_user_game_bucks")
public class SirfUserGameBucks extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sirf_user_game_bucks_Id")
    private long sirfUserGameBucksId;

    @Column(name = "game_balance")
    private BigDecimal userCreditBalance;

    @OneToMany(mappedBy = "sirfUserCreditBetTransactionPublisher", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<BetTransactionPublisher> betTransactionPublishers;

}
