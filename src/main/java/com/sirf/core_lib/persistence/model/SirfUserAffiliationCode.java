package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "sirf_usr_affltn_cd")
public class SirfUserAffiliationCode extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "affltn_cd_Id")
    private long affiliationCodeId;

    @Column(name = "affltn_cd", columnDefinition = "varchar(36) NOT NULL",updatable = false, nullable = false)
    private String affiliationCode;

    @Column(name = "sgnp_count")
    private Integer signupCount;

    @Column(name = "affltn_bns_awrd")
    private BigDecimal bonusAwarded;

    @Column(name = "affltn_description")
    private String affiliationDescription;

    @Column(name = "affltn_start_dt")
    private Timestamp startDate;

    @Column(name = "affltn_expiry_dt")
    private Timestamp expiryDate;

    @Column(name = "affltn_rfrl_amt")
    private BigDecimal referralAmount;

    @Override
    public String toString() {
        return "SirfUserAffiliationCode{" + "affiliationCodeId=" + affiliationCodeId + ", affiliationCode='" + affiliationCode + '\'' + ", signupCount=" + signupCount + ", bonusAwarded=" + bonusAwarded + ", affiliationDescription='" + affiliationDescription + '\'' + ", startDate=" + startDate + ", expiryDate=" + expiryDate + ", referralAmount=" + referralAmount + '}';
    }
}