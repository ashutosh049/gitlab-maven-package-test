package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Role implements Serializable {

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "role_id", nullable = false)
	private Long roleId;

	@Column(name = "role_name", nullable = false)
	private String roleName;

	@JsonIgnore
	@ManyToMany(mappedBy = "roles", fetch=FetchType.LAZY)
	private Collection<SirfUser> sirfUsers;

	@JsonIgnore
	@ManyToMany(cascade={CascadeType.MERGE}, fetch=FetchType.EAGER)
	@JoinTable(
			name = "roles_privileges",
			joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id"),
			inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "privilege_id"))
	private Collection<Privilege> privileges;

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Role{");
		sb.append("roleId=").append(roleId);
		sb.append(", roleName='").append(roleName).append('\'');
		sb.append(", privileges=").append(privileges.toString());
		sb.append('}');
		return sb.toString();
	}
}