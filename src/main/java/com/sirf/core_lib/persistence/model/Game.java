package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.GameBetStatus;
import com.sirf.core_lib.persistence.model.types.GameStatus;
import com.sirf.core_lib.persistence.model.types.GameSuperType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "game")
public class Game extends AbstractTimestampEntity
{

    @Id
    @Column(name = "game_id")
    private String gameId;

    @Enumerated(EnumType.STRING)
    private GameSuperType gameSuperType;

    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private GameBetStatus gameBetStatus = GameBetStatus.INFANT;

    private long gameStartTime;

    private String tournamentName;

    private String streamLink;

    private String platformId;

    private String gameMode;

    private long mapId;

    private String gameType;

    private long gameLength;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private BetGameStat betGameStat;

    @OneToMany(mappedBy = "betGame", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    private Collection<Bet> gameBet;

    @OneToMany(mappedBy = "gamePotGame", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    private Collection<GamePot> gamePots;

    @OneToMany(mappedBy = "teamGame", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private List<Team> teams;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Game game = (Game) o;
        return gameId == game.gameId && Objects.equals(platformId, game.platformId) && Objects.equals(gameType,
                game.gameType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, platformId, gameType);
    }

    @Override
    public String toString() {
        return "Game{" + "gameId=" + gameId + ", gameSuperType=" + gameSuperType + ", gameStatus=" + gameStatus + ", " +
                "platformId='" + platformId + '\'' + ", gameType='" + gameType + '\'' + '}';
    }
}
