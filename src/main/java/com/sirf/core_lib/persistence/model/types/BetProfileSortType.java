package com.sirf.core_lib.persistence.model.types;

import org.springframework.data.domain.Sort;

public enum BetProfileSortType {

    NONE("game_super_type", Sort.Direction.ASC),
    WINS("winning", Sort.Direction.DESC),
    LOSSES("winning", Sort.Direction.ASC);

    private String sortByField;
    private Sort.Direction direction;

    BetProfileSortType(String sortByField, Sort.Direction direction) {
        this.sortByField = sortByField;
        this.direction = direction;
    }

    public String getSortByField() {
        return sortByField;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

}
