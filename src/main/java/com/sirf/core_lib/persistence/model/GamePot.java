/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.GameSuperType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "game_pot")
public class GamePot extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "game_pot_id")
    private Long gamePotId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "game_id")
    private Game gamePotGame;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "bet_cat_id")
    private BetCategory gamePotBetCategory;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GameSuperType gameSuperType;

    private BigDecimal poolYes;

    private BigDecimal poolNo;

    private BigDecimal stake;

    private BigDecimal pot;

    private BigDecimal sirfPot;


}
