/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model.types;

import java.util.Arrays;
import java.util.NoSuchElementException;

public enum BetTeam {
    BLUE(100),
    RED(200);

    private int teamId;

    public int getTeamId() {
        return teamId;
    }


    BetTeam(int teamId) {
        this.teamId = teamId;
    }

    public static BetTeam getTeamById(Integer teamId) {
        return Arrays.stream(BetTeam.values()).filter(team -> {
            return team.getTeamId()==teamId;
        }).findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Unknown Team Id: %d",teamId)));
    }
}
