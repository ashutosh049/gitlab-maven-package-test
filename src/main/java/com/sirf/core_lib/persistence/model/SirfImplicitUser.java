package com.sirf.core_lib.persistence.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SirfImplicitUser {
        String email;
        String userName;
        String password;
        List<String> roles;
    }