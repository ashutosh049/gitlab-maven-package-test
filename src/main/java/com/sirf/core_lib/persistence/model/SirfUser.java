package com.sirf.core_lib.persistence.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "sirf_user")
public class SirfUser extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "sirf_user_id", columnDefinition = "varchar(36) NOT NULL",updatable = false, nullable = false)
    private String userId;

    @NotBlank(message = "{sirf.user.email.required}")
    @Pattern(regexp="^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "{sirf.user.email.invalid}")
    @Column(name = "email", nullable = false, unique = true, updatable = false)
    private String email;

    @NotBlank(message = "{sirf.user.username.required}")
    @Size(min=6, message="{sirf.user.username.min.length}")
    @Size(max=20, message="{sirf.user.username.max.length}")
    @Column(name = "username", nullable = false, unique = true, updatable = false)
    @Pattern(regexp="^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", message = "{sirf.user.username.invalid}")
    private String userName;

    /**
     * TODO
     * Spring bCrypt password encoder encodes the given plain string and hashes the password and store
     * it as a random 60 characters which violates the max size constraints of 20
     *
     * FIXME
     *  Using:- spring.jpa.properties.javax.persistence.validation.mode=none
     *  @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*_])(?!.*[`~^=+\\/\\\\\\[\\]?<>():;{}-])(?=\\S+$).{8,20}$", message = "Invalid Password")
     *
     *  @see <p>
     *      https://stackoverflow.com/questions/26764532/how-to-disable-hibernate-validation-in-a-spring-boot-project
     *      https://stackoverflow.com/questions/52460568/could-not-commit-jpa-transaction-nested-exception-is-javax persistence-rollback
     *      https://stackoverflow.com/questions/25851889/disable-hibernate-validation-for-merge-update
     *      </p>
     * */
    @NotBlank(message = "{sirf.user.password.required}")
    @Size(min=8, message="{sirf.user.password.min.length}")
    @Size(max=20, message="{sirf.user.password.max.length}")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*_])(?!.*[`~^=+/?<>():;-])(?=\\S+$).{8,20}$", message = "{sirf.user.password.invalid}")
    @Column(name = "password", columnDefinition = "varchar(255) NOT NULL", nullable = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "status", nullable = false)
    private String userStatus;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sirf_user_credit_Id")
    private SirfUserCredit sirfUserCredit = new SirfUserCredit();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "affltn_cd_Id")
    private SirfUserAffiliationCode sirfUserAffiliationCode;

    //@JsonIgnore
    @ManyToMany(cascade={CascadeType.MERGE}, fetch=FetchType.EAGER)
    @JoinTable(name = "users_roles",
        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "sirf_user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id"))
    private Collection<Role> roles;

    @OneToMany(mappedBy = "betSirfUser", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<Bet> betTransactions;

    @OneToMany(mappedBy = "sirfUserBetTransactionPublisher", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<BetTransactionPublisher> betTransactionPublishers;

    @OneToMany(mappedBy = "sirfUserImageDataSirfUser", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<SirfUserImageData> sirfUserImageData;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "group_Id")
    private SirfFABGroup sirfFABGroup;

    @Transient
    @JsonProperty("privileges")
    private List<Privilege> privileges;

    @OneToMany(mappedBy = "buyInSirfUser", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<BuyIn> buyIns;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SirfUser{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", enabled=").append(enabled);
        sb.append(", roles=").append(roles.toString());
        sb.append('}');
        return sb.toString();
    }
}
