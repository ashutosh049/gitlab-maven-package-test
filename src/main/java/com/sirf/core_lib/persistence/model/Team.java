package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "team")
public class Team extends AbstractTimestampEntity implements Comparable<Team>
{

    @EmbeddedId
    private TeamId id;

    private String teamName;

    private String flagName;

    private String logo;

    private boolean win;

    @JsonIgnore
    @MapsId("gameId")
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game teamGame;

    @Override
    public int compareTo(Team o) {
        return this.getId().getTeamId().compareTo(o.getId().getTeamId());
    }
}
