package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Privilege implements Serializable {
  
    @Id
    @Column(name = "privilege_id", nullable = false)
    private Integer privilegeId;

    @Column(name = "privilege_name", nullable = false)
    private String privilegeName;

    @JsonIgnore
    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Privilege privilege = (Privilege) o;

        if (!privilegeId.equals(privilege.privilegeId))
            return false;
        return privilegeName.equals(privilege.privilegeName);
    }

    @Override
    public int hashCode() {
        int result = privilegeId.hashCode();
        result = 31 * result + privilegeName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Privilege{");
        sb.append("privilegeId=").append(privilegeId);
        sb.append(", privilegeName='").append(privilegeName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}