package com.sirf.core_lib.persistence.model.types;

public enum BetMetaType {

    META_TYPE_TEAM(1),
    META_TYPE_PARTICIPANT(2);

    BetMetaType(Integer id) {
    }
}
