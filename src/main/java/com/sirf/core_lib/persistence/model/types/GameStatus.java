package com.sirf.core_lib.persistence.model.types;

public enum GameStatus {

    COMPLETE, LIVE, UPCOMING, NOTLIVE, NOTUPCOMING, NOTFINISHED, FINISHED;

}
