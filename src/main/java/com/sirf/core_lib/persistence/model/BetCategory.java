/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.BetSlab;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "bet_category")
public class BetCategory extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bet_cat_id")
    private Long betCategoryId;

    @Column(name = "game_id")
    private String gameId;

    @Column
    private String betStatement;

    @Enumerated
    @JsonIgnore
    @Column
    private BetSlab betSlab;

    @JsonIgnore
    private Integer teamId;

    @JsonIgnore
    private String teamName;

    @JsonIgnore
    private boolean timeBound;

    @JsonIgnore
    private int timeInMins;

    @OneToMany(mappedBy = "gamePotBetCategory", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<GamePot> gamePots;

    @OneToMany(mappedBy = "betBetCategory", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    private List<Bet> bets;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof BetCategory))
            return false;

        BetCategory that = (BetCategory) o;

        return new EqualsBuilder()
                .append(getTeamId(), that.getTeamId())
                .append(getGameId(), that.getGameId())
                .append(getBetSlab(), that.getBetSlab())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getGameId()).append(getBetSlab()).append(getTeamId()).toHashCode();
    }
}
