/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.BetStatus;
import com.sirf.core_lib.persistence.model.types.GameSuperType;
import com.sirf.core_lib.persistence.model.types.Standings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class BuyIn extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "buy_in_id", columnDefinition = "varchar(36) NOT NULL",updatable = false, nullable = false)
    private String buyInId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GameSuperType gameSuperType;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JsonIgnore
    private SirfUser buyInSirfUser;

    @Column(nullable = false)
    private BigDecimal buyInAmount;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private BetStatus betStatus;

    @Column(nullable = false)
    private BigDecimal allocatedSirfBucks;

    @Column(nullable = false)
    private BigDecimal availableSirfBucks;

    @Column(nullable = false)
    private Boolean winStatus;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private Standings standing;

    @Column(nullable = false)
    private BigDecimal payout;

    @OneToMany(mappedBy = "betBuyIn", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<Bet> bets;

    @Override
    public String toString() {
        return "BuyIn{" + "buyInId='" + buyInId + '\'' + ", game=" + game + ", buyInAmount=" + buyInAmount + ", " +
                "betStatus=" + betStatus + ", allocatedSirfBucks=" + allocatedSirfBucks + ", availableSirfBucks=" + availableSirfBucks + ", standing=" + standing + ", payout=" + payout + '}';
    }


}
