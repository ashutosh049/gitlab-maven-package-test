/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.BetStatus;
import com.sirf.core_lib.persistence.model.types.GameSuperType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "bet")
public class Bet extends AbstractTimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sirf_bet_id", updatable = false, nullable = false)
    private Long betId;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game betGame;

    @ManyToOne
    @JoinColumn(name = "sirf_user_id")
    @JsonIgnore
    private SirfUser betSirfUser;

    @ManyToOne
    @JoinColumn(name = "bet_cat_id")
    private BetCategory betBetCategory;

    @Column(nullable = false)
    private BigDecimal stake;

    @Column(nullable = false)
    @JsonIgnore
    private BigDecimal poolHolding;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private BetStatus betStatus;

    @Column(nullable = false)
    private Boolean betWinStatus;

    @Column(nullable = false)
    private BigDecimal winning;

    @OneToMany(mappedBy = "betBetTransactionPublisher", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    private Collection<BetTransactionPublisher> betTransactionPublishers;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GameSuperType gameSuperType;

    private boolean stmtSelection;

    @ManyToOne
    @JoinColumn(name = "buy_in_id")
    @JsonIgnore
    private BuyIn betBuyIn;

}
