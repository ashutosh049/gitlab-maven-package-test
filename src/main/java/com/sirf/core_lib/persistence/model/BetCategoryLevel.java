package com.sirf.core_lib.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bet_category_level")
public class BetCategoryLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bet_cat_level_id")
    private Long betCategoryLevelId;

    @Column(name = "bet_cat_level_name")
    private String betCategoryLevelName;

    @Column(name = "bet_cat_level_description")
    private String betCategoryLevelDescription;

    /*@OneToMany(mappedBy = "betCategoryLevel", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private List<BetCategory> betCategories;*/

}
