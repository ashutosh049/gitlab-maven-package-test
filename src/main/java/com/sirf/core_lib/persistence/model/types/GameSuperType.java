package com.sirf.core_lib.persistence.model.types;

import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

public enum GameSuperType {

    ALL{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    },
    LOL{
        public List<String> getRegions() {
            //return Arrays.asList("BR1", "EUN1", "EUW1", "JP1", "KR", "LA1", "LA2", "NA1", "OC1", "TR1", "RU", "PBE1");
            return Arrays.asList("NA1");
        }
    }, CS{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, OW{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, DOTA2{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, DOTA{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, HOTS{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, SC2{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, CSGO{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    }, HS{
        public List<String> getRegions() {
            return Arrays.asList();
        }
    };

    private List<String> regions;

    public abstract List<String> getRegions();

    /*public static GameSuperType getGameSuperTypeByValue(final String argGameSuperType){
        try{
            return GameSuperType.valueOf(argGameSuperType.trim().toUpperCase());
        }catch (IllegalArgumentException e){
            return ResponseEntity.badRequest().body(String.format("Invalid superType : %s", argGameSuperType));
        }
    }*/

}
