/*
 * Copyright (c) 2019, Sirf and/or its affiliates. All rights reserved.
 * Unauthorized copying/use of this file, via any medium is strictly prohibited
 * SIRF PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.sirf.core_lib.persistence.model.types;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.NoSuchElementException;

public enum BetSlab {

    WINS(0, false, 0){

        @Override
        public String buildBetStatement(final String teamName) {
            return String.format("%s wins",teamName);
        }

    }/*, WIN_WITHIN_10_MINS(1, true, 10){

        @Override
        public String buildBetStatement(final String teamName) {
            return String.format("%s wins within 10 mins",teamName);
        }

    }, WIN_WITHIN_20_MINS(2, true, 20){

        @Override
        public String buildBetStatement(final String teamName) {
            return String.format("%s wins within 20 mins",teamName);
        }

    }, WIN_WITHIN_30_MINS(3, true, 30){

        @Override
        public String buildBetStatement(final String teamName) {
            return String.format("%s wins within 30 mins",teamName);
        }

    }*/;

    private int betSlabId;
    private boolean timeBound;
    private int timeInMins;

    public abstract String buildBetStatement(String teamName);

    public int getBetSlabId() {
        return betSlabId;
    }

    public void setBetSlabId(int betSlabId) {
        this.betSlabId = betSlabId;
    }

    public boolean isTimeBound() {
        return timeBound;
    }

    public void setTimeBound(boolean timeBound) {
        this.timeBound = timeBound;
    }

    public int getTimeInMins() {
        return timeInMins;
    }

    public void setTimeInMins(int timeInMins) {
        this.timeInMins = timeInMins;
    }

    BetSlab(int betSlabId, boolean timeBound, int timeInMins) {
        this.betSlabId = betSlabId;
        this.timeBound = timeBound;
        this.timeInMins = timeInMins;
    }

    public static BetSlab getGameBetStatementById(Integer Id){
        return Arrays.stream(BetSlab.values())
                .filter(stmt_ -> stmt_.getBetSlabId()==Id)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Unknown GameBetStatementId: " + Id.toString()));
    }
}
