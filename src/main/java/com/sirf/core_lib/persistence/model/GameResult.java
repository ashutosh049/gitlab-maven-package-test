package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sirf.core_lib.persistence.model.types.GameBetStatus;
import com.sirf.core_lib.persistence.model.types.GameStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/*
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
*/
//@Entity
//@Table(name = "game_result")
public class GameResult extends AbstractTimestampEntity
{

    /*@Id
    @Column(name = "game_id")
    private long gameId;

    @ManyToOne
    @JoinColumn(name = "room_id")
    @JsonIgnore
    Room room;

    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @Enumerated(EnumType.STRING)
    private GameBetStatus gameBetStatus = GameBetStatus.INFANT;

    private long gameStartTime;

    private String platformId;

    private String gameMode;

    private long mapId;

    private String gameType;

    private long gameLength;

    @OneToOne(cascade = CascadeType.ALL)
    private BetGameStat betGameStat;

    @OneToMany(mappedBy = "game", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<Bet> betTransactions;*/

}
