package com.sirf.core_lib.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "participant")
public class Participant {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "sirf_user_id", columnDefinition = "varchar(36) NOT NULL",updatable = false, nullable = false)
    private String sirfParticipantId;

    private long profileIconId;

    private long championId;

    private String summonerName;

    private boolean bot;

    private long spell1Id;

    private long spell2Id;

    private long teamId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "game_id")
    private Game participantsGame;
}
