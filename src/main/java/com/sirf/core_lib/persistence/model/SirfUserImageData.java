package com.sirf.core_lib.persistence.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "sirf_user_avatar")
public class SirfUserImageData {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "sirf_user_avatar_Id")
    private String sirfUserImageId;

    @ManyToOne
    @JoinColumn(name = "sirf_user_id")
    @JsonIgnore
    private SirfUser sirfUserImageDataSirfUser;

    private String imageType;
    private String fileName;
    private String fileType;
    private Long fileSize;
    @Lob
    private byte[] data;
    @Transient
    private String fileAccessUri;

    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String uploadId;

}