package com.sirf.core_lib.persistence.model.types;

public enum SirfAuthorities {
    ADMIN_ACCOUNT(){
        @Override
        public String getAccountType(){
            return "ADMIN";
        }

    },
    TEST_ACCOUNT(){
        @Override
        public String getAccountType(){
            return "TEST";
        }
    },
    PUNTER_ACCOUNT(){
        @Override
        public String getAccountType(){
            return "PUNTER";
        }
    };

    public abstract String getAccountType();
}
